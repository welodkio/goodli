import win32gui

win2find = '1.jpg'          # change this whenever

def winEnumHandler( hwnd, ctx ):
    if win32gui.IsWindowVisible( hwnd ):
        ctx.append(win32gui.GetWindowText(hwnd))

windows = []
win32gui.EnumWindows( winEnumHandler, windows )

condition = any([win2find in p for p in windows])
print (f"{win2find} is{'' if condition else 'not'} open")